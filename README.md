

### Tag Emulator ###

this emulates​ the protocal used by the NTAG NFC tags and an associated​ NFC tag reader

### How do I get set up? ###

incomplete arduino example for usage of this library
```
void writeRequest(){
  if(recivedRegAddr){
    unsigned char val=0;
    reader_pns512_reg_request_read(&reader,regAddr,&val);
    Wire.write(val);
    recivedRegAddr=false;
  }
}

void readRequest(int numBytes){
  lastNumBytes=numBytes;
  if( numBytes == 2){
    char regAddr=Wire.read();
    unsigned char val=Wire.read();
    reader_pns512_reg_request_write(&reader,regAddr,&val);
  }
  else if(numBytes == 1){
    regAddr=Wire.read();
    recivedRegAddr=true;
  }
}

void reader_timer_stop(ReaderTimer* timerContext){}
void reader_timer_start(ReaderTimer* timerContext){}

void loop() {
  reader_pns512_process_TX_RX(&reader);
}
```

### Contribution ###

what to Contribute? sure create a pull request
