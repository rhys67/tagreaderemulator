#include "../src/reader/reader_pns512.h"
#include "../src/tag/tag_ntag213.h"
#define ASSERT_TRUE(MSG,X) if(! (X) ) {printf(MSG " Failed!\n"); return 1;}else{printf(MSG " Sucsess!\n");}
#define ASSERT(MSG,X,Y) do{int xTmp=X;int yTmp=Y; if( (xTmp != yTmp) ) { printf(MSG " Failed! got:%d expected %d\n",yTmp,xTmp); return 1;}else{printf(MSG " Sucsess!\n");}}while(0)
PNSReader state;
int runTests();
void setReg(char regAddr, char val) {
	reader_pns512_process_reg(&state,regAddr, ACTION_TYPE_REG_WRITE, &val);
}

unsigned char getReg(char regAddr) {
	char val[1] = "";
	reader_pns512_process_reg(&state, regAddr, ACTION_TYPE_REG_READ, val);

	return val[0];
}
void writeToBuffer(char* data, short dataLen) {

	for (int i = 0; i<dataLen; i++) {
		setReg( PNS512_REG_FIFO_DATA, data[i]);
	}
}
int main(){
	reader_pns512_init(&state);
	int result = 0;
	for (int i = 0; i < 1000; i++) {
		result = runTests();
		if (result) {
			return result;
		}
	}

	return 0;
}
int runTests() {
	setReg(PN512_REG_COMMAND_REG, PNS512_REG_COMMAND_BITS_CONFIGURE);
	ASSERT_TRUE("Command Register Set", state.resisters.command == PNS512_REG_COMMAND_BITS_CONFIGURE);

	char data[2] = { NTAG_CMD_READ,0 };
	writeToBuffer(data, sizeof(data));
	ASSERT("FIFO Buffer Length Check", getReg(PN512_REG_FIFOLEVEL_REG), sizeof(data));

	ASSERT_TRUE("FIFO Buffer Read", getReg(PNS512_REG_FIFO_DATA) == NTAG_CMD_READ);//should return first byte written
																				   //clear buffer
	setReg(PN512_REG_FIFOLEVEL_REG, 0);
	//check it's empty
	ASSERT("FIFO Buffer check empty", getReg(PN512_REG_FIFOLEVEL_REG), 0);
	//read card
	writeToBuffer(data, sizeof(data));
	setReg(PN512_REG_COMMAND_REG, PNS512_REG_COMMAND_BITS_TRANSCEIVE);
	ASSERT_TRUE("current command is TRANSCEIVE", state.resisters.command == PNS512_REG_COMMAND_BITS_TRANSCEIVE);
	//FIFO should now contains 4 pages or 16Bytes
	ASSERT_TRUE("FIFO Buffer contains TAG response", getReg(PN512_REG_FIFOLEVEL_REG) == 16);
	//ASSERT_TRUE("TAG READ response", getReg(PNS512_REG_FIFO_DATA) == Ntag_pages[0][0]);//should return first first page first byte
	//check all returned data
	for (int p = 0; p < NTAG_NUM_PAGES_PER_READ; p++) {
		for (int b = 0; b < NTAG_NUM_BYTES_PER_PAGE; b++) {
			ASSERT("TAG READ response", getReg(PNS512_REG_FIFO_DATA), (unsigned char)Ntag_pages[p][b]);
		}
	}
	ASSERT("FIFO Buffer check empty", getReg(PN512_REG_FIFOLEVEL_REG), 0);

	char authRequest[5] = { NTAG_CMD_PWD_AUTH ,0x1,0x2,0x3,0x4 };
	writeToBuffer(authRequest, sizeof(authRequest));
	//process command
	setReg(PN512_REG_COMMAND_REG, PNS512_REG_COMMAND_BITS_TRANSCEIVE);
	for (int i = 0; i < NTAG_AUTH_PACK_SIZE; i++) {
		ASSERT("TAG auth response", getReg(PNS512_REG_FIFO_DATA), (unsigned char)Ntag_pages[NTAG_AUTH_PACK_PAGE_INDEX][NTAG_AUTH_PACK_PAGE_BYTE_INDEX + i]);
	}
	ASSERT_TRUE("FIFO Buffer check empty", getReg(PN512_REG_FIFOLEVEL_REG) == 0);

	char selectC1Request[2] = { NTAG_CMD_CL1 ,NTAG_CMD_ANTICOLLISION };
	writeToBuffer(selectC1Request, sizeof(selectC1Request));
	setReg(PN512_REG_COMMAND_REG, PNS512_REG_COMMAND_BITS_TRANSCEIVE);
	ASSERT("FIFO Buffer contains NTAG_CMD_ANTICOLLISION response",5, getReg(PN512_REG_FIFOLEVEL_REG));

	return 0;
	char detect[1] = { NTAG_CMD_REQA };
	for (int i = 0; i < 5; i++) {
		writeToBuffer(detect, sizeof(detect));
		setReg(PN512_REG_COMMAND_REG, PNS512_REG_COMMAND_BITS_TRANSCEIVE);
		ASSERT("FIFO Buffer contains NTAG_CMD_REQA response", 2, getReg(PN512_REG_FIFOLEVEL_REG));
	}
}