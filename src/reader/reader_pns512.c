#include "reader_pns512.h"
#include "../reader_tag_io_buffer.h"
#include "../tag/tag_ntag213.h"
#include "../reader_config.h"

static void process_command_reg(PNSReader * state, char cmd);
static void set_interrupt_flag(PNSReader * state, char interuptFlag);
static void reader_pn512_reset_command(PNSReader * state);
static void reader_get_config_callback(int8_t errCode,uint8_t data,void* context);
static void check_FIFO_level(PNSReader * state);

void reader_pns512_init(PNSReader * state) {
    
    state->lastBufferLevel=255;
    state->currentWaterLevelAlerts=0;
    reader_tag_io_buffer_clear();
    
    //reader_config_get(READER_CONFIG_INDEX_1,reader_get_config_callback,NULL);
}

void reader_pns512_reg_request_read(PNSReader * state,char regAddr,unsigned char* data){
    switch( regAddr ){
        case PN512_REG_FIFODATA_REG:
            data[0]=reader_tag_io_buffer_read_byte();
            return;
        case PN512_REG_VERSION_REG:
            data[0]=0x82;
            return;
        case PN512_REG_FIFOLEVEL_REG:
            data[0] = (unsigned char) reader_tag_io_buffer_level();
            return;
        case PN512_REG_STATUS1_REG:
        {
            /* append the water level flags if required*/

            unsigned char bufferLevel = reader_tag_io_buffer_level();
            unsigned char waterLevel = state->registors[PN512_REG_WATER_LEVEL_REG];
            char status1 = state->registors[PN512_REG_STATUS1_REG];

            if (FIFO_BUFFER_SIZE - bufferLevel < state->registors[PN512_REG_WATER_LEVEL_REG]) {
                status1 |= PNS512_REG_STATUS1_BITS_HI_ALERT;
            }
            if (bufferLevel < state->registors[PN512_REG_WATER_LEVEL_REG] ) {
                status1 |= PNS512_REG_STATUS1_BITS_LOW_ALERT;
            }

            //interrupts need checking
            if (state->registors[PN512_REG_COMM_IRQ_REG] != 0) {
                status1 |= PNS512_REG_STATUS1_BITS_IRQ;
            }
            
            
            data[0] = status1;
            return;
        }
    }
    //catch all for registors not requiring specific 
    data[0] = state->registors[regAddr];;
}

void reader_pns512_reg_request_write(PNSReader * state,char regAddr,unsigned char* data){
    char val = data[0];
    switch( regAddr ){
        case PN512_REG_FIFODATA_REG:
            reader_tag_io_buffer_write_byte(data[0]);
            return;
        case PN512_REG_COMMAND_REG:
            process_command_reg(state, data[0]);
            break;
        case PN512_REG_FIFOLEVEL_REG:
            reader_tag_io_buffer_clear();
            return;
        case PN512_REG_BIT_FRAMING_REG:
            if( data[0] & PNS512_REG_BITFRAMING_BITS_START_SEND && state->registors[PN512_REG_COMMAND_REG] & PNS512_REG_COMMAND_BITS_TRANSCEIVE){
                state->nextOperation|=PENDING_OPERATION_TRANSMIT;
                state->nextOperation|=PENDING_OPERATION_RECEIVE;

                //we don't want to save this flag as its write only
                val &= ~PNS512_REG_BITFRAMING_BITS_START_SEND;
            }
            break;
        case PN512_REG_TX_CONTROL_REG:
            if(data[0] & PNS512_REG_TX_CONTROL_BITS_CHECK_RF){
                val &= ~PNS512_REG_TX_CONTROL_BITS_CHECK_RF;
            }
            break;
        case PN512_REG_CONTROL_REG:
            //handle start and stop of the timer
            if (val & PNS512_REG_CONTROL_BITS_TSTOP_NOW && (state->registors[PN512_REG_STATUS1_REG]  & PNS512_REG_STATUS1_BITS_TRUNNING) ){

                reader_timer_stop(&state->timer);

                state->registors[PN512_REG_STATUS1_REG] &= ~PNS512_REG_STATUS1_BITS_TRUNNING;
                val &= ~PNS512_REG_CONTROL_BITS_TSTOP_NOW;
            }
            else if (val & PNS512_REG_CONTROL_BITS_TSTART_NOW) {
                                    short counterReloadVal=(short)((state->registors[PN512_REG_TRELOAD_HIGH_REG] << 8) + (state->registors[PN512_REG_TRELOAD_LOW_REG]));
                short devider=(state->registors[PN512_REG_TPRESCALER_LOW_REG] << 8) + ((state->registors[PN512_REG_TMODE_REG] & PNS512_REG_TMODE_PRESCALER_MASK) & 0xFF);

                state->timer.preScaler=devider;
                state->timer.reload=counterReloadVal;
                reader_timer_start(&state->timer);
                state->registors[PN512_REG_STATUS1_REG] |= PNS512_REG_STATUS1_BITS_TRUNNING;
                val &= ~PNS512_REG_CONTROL_BITS_TSTART_NOW;
            }
            break;
        case PN512_REG_COMM_IRQ_REG:
            if (!(val & PN512_REG_COMM_IRQ_BIT_CLEAR)) {
                state->registors[PN512_REG_COMM_IRQ_REG] &= ~val;
            } else {
                val &= ~PN512_REG_COMM_IRQ_BIT_CLEAR;
                state->registors[PN512_REG_COMM_IRQ_REG] |= val;
            }
            return;
        case PN512_REG_DIV_IRQ_REG:
            if (!(val & PN512_REG_COMM_IRQ_BIT_CLEAR)) {
                state->registors[PN512_REG_DIV_IRQ_REG] &= ~val;
            } else {
                val &= ~PN512_REG_COMM_IRQ_BIT_CLEAR;
                state->registors[PN512_REG_DIV_IRQ_REG] |= val;
            }
            return;
    }
    
    char* reg = &state->registors[regAddr];
    if(reg != 0){
        *reg = val;
    }
    
}
static void process_command_reg(PNSReader * state, char cmd) {
    switch (cmd & PNS512_REG_COMMAND_CMD_MASK) {
        case PNS512_REG_COMMAND_BITS_TRANSMIT:
            DEBUG_LOG("PNS512_REG_COMMAND_BITS_TRANSMIT");
            /* call the tag command proceser*/
            state->nextOperation|=PENDING_OPERATION_TRANSMIT;
            break;
            
        case PNS512_REG_COMMAND_BITS_RECEIVE:
            state->nextOperation|=PENDING_OPERATION_RECEIVE;
            break;
        /**
         * commands not current implemented  
        case PNS512_REG_COMMAND_BITS_NO_CHANGE:
            DEBUG_LOG("PNS512_REG_COMMAND_BITS_NO_CHANGE");

            break;

        case PNS512_REG_COMMAND_BITS_TRANSCEIVE:
            break;

        case PNS512_REG_COMMAND_BITS_AUTO_COLL:
            DEBUG_LOG("PNS512_REG_COMMAND_BITS_AUTO_COLL");
            break;

        case PNS512_REG_COMMAND_BITS_MF_AUTHENT:
            DEBUG_LOG("PNS512_REG_COMMAND_BITS_MF_AUTHENT");

            break;

        case PNS512_REG_COMMAND_BITS_SOFT_RESET:
            DEBUG_LOG("PNS512_REG_COMMAND_BITS_SOFT_RESET");

            break;
        case PNS512_REG_COMMAND_CMD_IDLE:
            DEBUG_LOG("PNS512_REG_COMMAND_CMD_IDLE");

            break;

        case PNS512_REG_COMMAND_BITS_CONFIGURE:
            DEBUG_LOG("PNS512_REG_COMMAND_BITS_CONFIGURE");

            break;

        case PNS512_REG_COMMAND_BITS_GENERATE_RANDOM_ID:
            DEBUG_LOG("PNS512_REG_COMMAND_BITS_GENERATE_RANDOM_ID");

            break;

        case PNS512_REG_COMMAND_BITS_CALC_CRC:
            DEBUG_LOG("PNS512_REG_COMMAND_BITS_CALC_CRC");

            break;
        **/
    }
}

static void set_interrupt_flag(PNSReader * state, char interuptFlag) {
    state->registors[PN512_REG_COMM_IRQ_REG] |= interuptFlag;
}

static void reader_pn512_reset_command(PNSReader * state){
    if( state->registors[PN512_REG_COMMAND_REG] !=PNS512_REG_COMMAND_CMD_IDLE 
            && !(state->registors[PN512_REG_RX_MODE_REG] & PNS512_REG_RXMODE_BITS_RX_MULTIPLE)){
        
        state->registors[PN512_REG_COMMAND_REG]=PNS512_REG_COMMAND_CMD_IDLE;
        set_interrupt_flag(state,PNS512_REG_INTERRUPT_BIT_IDLEIEN);
    }
}

static void check_FIFO_level(PNSReader * state){
     unsigned char bufferLevel=reader_tag_io_buffer_level();
    
    if( state->lastBufferLevel != bufferLevel){
        unsigned char waterLevelAlerts=0;
        if (FIFO_BUFFER_SIZE - bufferLevel < state->registors[PN512_REG_WATER_LEVEL_REG]) {
            waterLevelAlerts |= PNS512_REG_STATUS1_BITS_HI_ALERT;
        }
        if (bufferLevel < state->registors[PN512_REG_WATER_LEVEL_REG] ) {
            waterLevelAlerts |= PNS512_REG_STATUS1_BITS_LOW_ALERT;
        }
        
        if( waterLevelAlerts != state->currentWaterLevelAlerts){
            
            if( waterLevelAlerts & PNS512_REG_STATUS1_BITS_HI_ALERT){
                set_interrupt_flag(state,PNS512_REG_COMLEN_BIT_HIALERTEN);
            }
            if( waterLevelAlerts & PNS512_REG_STATUS1_BITS_LOW_ALERT){
                set_interrupt_flag(state,PNS512_REG_COMLEN_BIT_LOALERIEN);
            }
            
            state->currentWaterLevelAlerts=waterLevelAlerts;
        }

        state->lastBufferLevel=bufferLevel;
    }
}
void reader_pns512_process_TX_RX(PNSReader * state){
    check_FIFO_level(state);
    if( state->nextOperation & PENDING_OPERATION_TRANSMIT ){
        state->nextOperation &= ~PENDING_OPERATION_TRANSMIT;
        if( state->readerConfigBits & READER_CONFIG_BITS_TAG_ENABLED ){
            char isActive=tag_runtime_data.state == MTAG_STATE_ACTIVE;
            tag_ntag213_process_buffer();
            if( state->registors[PN512_REG_TX_MODE_REG] & PNS512_REG_TXMODE_BITS_TX_CRC_EN && isActive){
               unsigned char crc[2]="";
               reader_tag_io_buffer_getCrc(crc);
               reader_tag_io_buffer_write(crc,sizeof(crc));
            }
        }else{ 
            reader_tag_io_buffer_clear();
            if(state->readerConfigBits & READER_CONFIG_BITS_TAG_ENABLED_TOGGLE){
                state->readerConfigBits|=READER_CONFIG_BITS_TAG_ENABLED;
                state->readerConfigBits&= ~READER_CONFIG_BITS_TAG_ENABLED_TOGGLE;
            }
        }
        set_interrupt_flag(state,PNS512_REG_INTERRUPT_BIT_TXIEN);
        //state->resisters.control_|=0x10;
        //reader_pn512_resetCommand(state);
        if( state->registors[PN512_REG_RX_MODE_REG] & PNS512_REG_TXMODE_BITS_TX_CRC_EN){
            state->registors[PN512_REG_STATUS1_REG]|=PNS512_REG_STATUS1_BITS_CRC_OK;
        }
    }
    if( state->nextOperation & PENDING_OPERATION_RECEIVE ){
        state->nextOperation &= ~PENDING_OPERATION_RECEIVE;
        set_interrupt_flag(state,PNS512_REG_INTERRUPT_BIT_RXIEN);
        //reader_pn512_resetCommand(state);
        //TODO
        if( state->registors[PN512_REG_RX_MODE_REG] & PNS512_REG_RXMODE_BITS_RX_CRC_EN){
            state->registors[PN512_REG_STATUS1_REG]|=PNS512_REG_STATUS1_BITS_CRC_OK;
        }
    }


}

void reader_timer_finished(void * state){
    PNSReader* reader=(PNSReader*)state;
    set_interrupt_flag(reader,PNS512_REG_COMLEN_BIT_TIMERIEN);
    reader->registors[PN512_REG_STATUS1_REG] &= ~PNS512_REG_STATUS1_BITS_TRUNNING;
}
