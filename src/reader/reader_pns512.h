#ifndef READER_PN512_H
#define READER_PN512_H
#include<stdbool.h>
/* why is this here */
/* Page 0: Command and Status */
#define PNS512_REG_Page 0x0 /* Selects the register page */
#define PNS512_REG_Command 0x1 /* Starts and stops command execution */
#define PNS512_REG_ComlEn 0x2 /* Controls bits to enable and disable the passing of Interrupt Requests */
#define PNS512_REG_DivlEn 0x3 /* Controls bits to enable and disable the passing of Interrupt Requests */
#define PNS512_REG_ComIrq 0x4 /* Contains Interrupt Request bits */
#define PNS512_REG_DivIrq 0x5 /* Contains Interrupt Request bits */
#define PNS512_REG_Error 0x6 /* Error bits showing the error status of the last command executed */
#define PNS512_REG_Status1 0x7 /* Contains status bits for communication */
#define PNS512_REG_Status2 0x8 /* Contains status bits of the receiver and transmitter */
#define PNS512_REG_FIFO_DATA 0x9 /* In- and output of 64 byte FIFO-buffer */
#define PNS512_REG_FIFOLevel 0xA /* Indicates the number of bytes stored in the FIFO */
#define PNS512_REG_WaterLevel 0xB /* Defines the level for FIFO under- and overflow warning */
#define PNS512_REG_Control 0xC /* Contains miscellaneous Control Registers */
#define PNS512_REG_BitFraming 0xD /* Adjustments for bit oriented frames */
#define PNS512_REG_Coll 0xE /* Bit position of the first bit collision detected on the RF-interface */

/* Page 1: Command */
#define PNS512_REG_Page 0x0 /* Selects the register page */
#define PNS512_REG_Mode 0x1 /* Defines general modes for transmitting and receiving */
#define PNS512_REG_TxMode 0x2 /* Defines the data rate and framing during transmission */
#define PNS512_REG_RxMode 0x3 /* Defines the data rate and framing during receiving */
#define PNS512_REG_TxControl 0x4 /* Controls the logical behavior of the antenna driver pins TX1 and TX2 */
#define PNS512_REG_TxAuto 0x5 /* Controls the setting of the antenna drivers */
#define PNS512_REG_TxSel 0x6 /* Selects the internal sources for the antenna driver */
#define PNS512_REG_RxSel 0x7 /* Selects internal receiver settings */
#define PNS512_REG_RxThreshold 0x8 /* Selects thresholds for the bit decoder */
#define PNS512_REG_Demod 0x9 /* Defines demodulator settings */
#define PNS512_REG_FelNFC1 0xA /* Defines the length of the valid range for the receive package */
#define PNS512_REG_FelNFC2 0xB /* Defines the length of the valid range for the receive package */
#define PNS512_REG_MifNFC 0xC /* Controls the communication in ISO/IEC 14443/MIFARE and NFC target mode at 106 kbit */
#define PNS512_REG_ManualRCV 0xD /* Allows manual fine tuning of the internal receiver */
#define PNS512_REG_TypeB 0xE /* Configure the ISO/IEC 14443 type B */
#define PNS512_REG_SerialSpeed 0xF /* Selects the speed of the serial UART interface */

/* Page 2: CFG */
#define PNS512_REG_Page 0x0 /* Selects the register page */
#define PNS512_REG_CRCResult 0x1 /* Shows the actual MSB and LSB values of the CRC calculation */
#define PNS512_REG_GsNOff 0x3 /* Selects the conductance of the antenna driver pins TX1 and TX2 for modulation, when the driver is switched off */
#define PNS512_REG_ModWidth 0x4 /* Controls the setting of the ModWidth */
#define PNS512_REG_TxBitPhase 0x5 /* Adjust the TX bit phase at 106 kbit */
#define PNS512_REG_RFCfg 0x6 /* Configures the receiver gain and RF level */
#define PNS512_REG_GsNOn 0x7 /* Selects the conductance of the antenna driver pins TX1 and TX2 for modulation when the drivers are switched on */
#define PNS512_REG_CWGsP 0x8 /* Selects the conductance of the antenna driver pins TX1 and TX2 for modulation during times of no modulation */
#define PNS512_REG_ModGsP 0x9 /* Selects the conductance of the antenna driver pins TX1 and TX2 for modulation during modulation */
#define PNS512_REG_TPrescaler 0xA /* Defines settings for the internal timer */
#define PNS512_REG_TMode 0xB /* Defines settings for the internal timer */
#define PNS512_REG_TReload 0xC /* Describes the 16-bit timer reload value (D) */
#define PNS512_REG_TCounterVal 0xE /* Shows the 16-bit actual timer value (E) */

/* Page 3: TestRegister */
#define PNS512_REG_Page 0x0 /* selects the register page */
#define PNS512_REG_TestSel1 0x1 /* General test signal configuration */
#define PNS512_REG_TestSel2 0x2 /* General test signal configuration and PRBS control */
#define PNS512_REG_TestPinEn 0x3 /* Enables pin output driver on 8-bit parallel bus (Note: For serial interfaces only) */
#define PNS512_REG_TestPin 0x4 /* ValueReg Defines the values for the 8-bit parallel bus when it is used as I/O bus */
#define PNS512_REG_TestBus 0x5 /* Shows the status of the internal testbus */
#define PNS512_REG_AutoTest 0x6 /* Controls the digital selftest  */

#define PNS512_REG_PAGE_SELECT 0x0
#define PNS512_REG_COMMAND 0x01
#define PNS512_REG_COMMI_RQ 0x4
#define PNS512_REG_STATUS1_REG 0x7
#define PNS512_REG_STATUS2_REG 0x8
#define PNS512_REG_BITFRAMING 0x0D
#define PNS512_REG_COLL 0x0E
#define PNS512_REG_PAGE 0x10
#define PNS512_REG_MODE 0x11
#define PNS512_REG_TxMODE 0x12
#define PNS512_REG_RxMODE 0x13
#define PNS512_REG_TxCONTROL 0x14
#define PNS512_REG_TxAUTO 0x15
#define PNS512_REG_TxSEL 0x16
#define PNS512_REG_RxSEL 0x17
#define PNS512_REG_RxTHRESHOLD 0x18
#define PNS512_REG_DEMOD 0x19
#define PNS512_REG_FELNFC1 0x1A
#define PNS512_REG_MIFNFC 0x1C
#define PNS512_REG_MANUALRCV 0x1D
#define PNS512_REG_TYPEB 0x1E
#define PNS512_REG_SERIALSPEED 0x1F
#define PNS512_REG_VERSION 0x37
#define PNS512_REG_TMODE 0x2A
#define PNS512_REG_TPRESCALER 0x2B

#define PNS512_VERSION_1 0x80

#define STATUS_RF_FREQ_OK 0x80
#define STATUS_CRC_OK 0x40
#define STATUS_CRC_READY 0x20
#define STATUS_REG_BITS_IRQ 0x10
#define STATUS_TRUNNING 0x8
#define STATUS_RF_ON 0x4
#define STATUS_HI_ALERT 0x2
#define STATUS_LO_ALERT 0x1


#define ADDRESS_MASK 0x3F
#define MODE_MASK 0x80
#define MODE_READ 0
#define MODE_WRITE 0x80

#define PNS512_REG_RxSEL_DEFAULT_MODE 0x84
#define PNS512_REG_TxCONTROL_DEFAULT_MODE 0x80;
#define	PNS512_REG_COMMAND_DEFAULT_MODE 0x20;
#define PNS512_REG_TMODE_DEFAULT_MODE 0x0
#define PNS512_REG_TPRESCALER_DEFAULT_MODE 0x0

//first 4 bits of mode reg are the high word of Prescaler
#define PNS512_REG_TMODE_PRESCALER_MASK 0xF 

#define PNS512_REG_BITFRAMING_BITS_START_SEND 0x80

/* command bits */
#define PNS512_REG_COMMAND_CMD_MASK 0xF
#define PNS512_REG_COMMAND_CMD_IDLE 0x0 /* cancal current command */
#define PNS512_REG_COMMAND_BITS_CONFIGURE 0x1
#define PNS512_REG_COMMAND_BITS_GENERATE_RANDOM_ID 0x2
#define PNS512_REG_COMMAND_BITS_CALC_CRC 0x3
#define PNS512_REG_COMMAND_BITS_TRANSMIT 0x4
#define PNS512_REG_COMMAND_BITS_NO_CHANGE 0x7
#define PNS512_REG_COMMAND_BITS_RECEIVE 0x8
#define PNS512_REG_COMMAND_BITS_TRANSCEIVE 0xC /* transmits data from FIFO buffer then actvates the reciver */
#define PNS512_REG_COMMAND_BITS_AUTO_COLL 0xD
#define PNS512_REG_COMMAND_BITS_MF_AUTHENT 0xE
#define PNS512_REG_COMMAND_BITS_SOFT_RESET 0xF

/* Control bits to enable and disable the passing of interrupt requests. */
#define PNS512_REG_COMMIEN 0x02
/* the signal on pin IRQ is inverted with respect to bit IRq */
#define PNS512_REG_COMLEN_BIT_IRQINV 0x80
#define PNS512_REG_INTERRUPT_BIT_TXIEN 0x40
#define PNS512_REG_INTERRUPT_BIT_RXIEN 0x20
#define PNS512_REG_INTERRUPT_BIT_IDLEIEN 0x10
#define PNS512_REG_COMLEN_BIT_HIALERTEN 0x8
#define PNS512_REG_COMLEN_BIT_LOALERIEN 0x4
#define PNS512_REG_COMLEN_BIT_ERRIEN 0x2
#define PNS512_REG_COMLEN_BIT_TIMERIEN 0x1

#define PN512_REG_COMM_IRQ_BIT_CLEAR 0x80

#define PNS512_REG_COMMIEN_DEFAULT_MODE 0x80

#define PNS512_REG_STATUS1_BITS_HI_ALERT 0x2
#define PNS512_REG_STATUS1_BITS_LOW_ALERT 0x1
#define PNS512_REG_STATUS1_BITS_TRUNNING 0x8
#define PNS512_REG_STATUS1_BITS_RF_ON 0x04
#define PNS512_REG_STATUS1_BITS_IRQ 0x10
#define PNS512_REG_STATUS1_BITS_CRC_READY 0x20
#define PNS512_REG_STATUS1_BITS_CRC_OK 0x40
#define PNS512_REG_STATUS1_BITS_CRC_RF_FREQ_OK 0x80

#define PNS512_REG_TX_CONTROL_BITS_CHECK_RF 0x4

#define PNS512_REG_CONTROL_BITS_TSTOP_NOW 0x80
#define PNS512_REG_CONTROL_BITS_TSTART_NOW 0x40

#define PNS512_REG_RXMODE_BITS_RX_CRC_EN 0x80
#define PNS512_REG_RXMODE_BITS_RX_MULTIPLE 0x01

#define PNS512_REG_TXMODE_BITS_TX_CRC_EN 0x80

#define READER_CONFIG_BITS_TAG_ENABLED 0x1
#define READER_CONFIG_BITS_TAG_ENABLED_TOGGLE 0x02
#define READER_CONFIG_INDEX_1 0x2
#define READER_LIB_VERSION "1.1"
#include<stdint.h>
#include<stdio.h>
#ifndef DEBUG_LOG
#define DEBUG_LOG(MSG) /* printf("RB:%d\n",__LINE__); */
#endif

typedef enum{
    PENDING_OPERATION_TRANSMIT=1,
    PENDING_OPERATION_RECEIVE=2
} PENDING_OPERATION;

#include "reader_timer.h"
#include "registers.h"
typedef struct{
    char registors[166];
    ReaderTimer timer;
    PENDING_OPERATION nextOperation;
    unsigned char lastBufferLevel;
    unsigned char currentWaterLevelAlerts;
    uint8_t readerConfigBits;
} PNSReader;

void reader_pns512_init(PNSReader* state);

void reader_pns512_reg_request_read(PNSReader * state,char regAddr,unsigned char* data);

void reader_pns512_reg_request_write(PNSReader * state,char regAddr,unsigned char* data);

void reader_pns512_process_TX_RX(PNSReader * state);

void reader_pns512_write_config(PNSReader* reader);

void reader_pns512_enter_upgrade_mode();
#endif