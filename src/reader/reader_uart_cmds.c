#include "reader_uart_cmds.h"
#include "../uart_cmds.h"
#include "reader_pns512.h"
#include "../reader_config.h"

void reader_uart_cmds_processCmd(UartCmdProcesser* cmd){
    cmd->currentCommand&= ~UART_CMDS_TYPE_READER;
    switch(cmd->currentCommand){
        case READER_UART_CMD_DISABLE_TAG:
            cmd->reader->readerConfigBits &= ~READER_CONFIG_BITS_TAG_ENABLED;
            //reader_pns512_write_config(cmd->reader);
            break;
        case READER_UART_CMD_ENABLE_TAG:
            cmd->reader->readerConfigBits |= READER_CONFIG_BITS_TAG_ENABLED;
            //reader_pns512_write_config(cmd->reader);
            break;
        case READER_UART_CMD_START_UPGRADE_MODE:
            reader_pns512_enter_upgrade_mode();
            break;
        case READER_UART_CMD_GET_VERSION_INFO:
        {
            uint8_t data[sizeof(VERSION)]=VERSION;
            cmd->writeResponse(data,sizeof(data));
        }
            break;
            
    }
}