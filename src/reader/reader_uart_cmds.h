#ifndef READER_UART_CMDS_H
#define READER_UART_CMDS_H
#include "../uart_cmds.h"
#define READER_UART_CMD_DISABLE_TAG 1
#define READER_UART_CMD_ENABLE_TAG 2
#define READER_UART_CMD_START_UPGRADE_MODE 5
#define READER_UART_CMD_GET_VERSION_INFO 6
#define READER_CONFIG_INDEX_TAG_ENABLED 2

#define VERSION READER_LIB_VERSION "\0" __DATE__ " " __TIME__ "\0"
static inline short reader_uart_cmds_getRequiredByteCount(uint8_t cmd){
    switch(cmd){
        case READER_UART_CMD_START_UPGRADE_MODE:
        case READER_UART_CMD_GET_VERSION_INFO:
        case READER_UART_CMD_DISABLE_TAG:
        case READER_UART_CMD_ENABLE_TAG:
            return 0;
        default:
           return UART_CMDS_UNKNOWN; 
    }
};
void reader_uart_cmds_processCmd(UartCmdProcesser* cmd);

#endif