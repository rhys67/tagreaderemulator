#ifndef READER_TIMER_H
#define READER_TIMER_H
typedef struct{
    uint16_t preScaler;
    uint16_t reload;
    volatile int32_t counter;
    char autoRestart;
} ReaderTimer;

void reader_timer_setTimerMode(ReaderTimer* timerContext,int timerMode);
void reader_timer_stop(ReaderTimer* timerContext);
void reader_timer_start(ReaderTimer* timerContext);

void reader_timer_finished(void* context);
#endif