#include "tag_uart_cmds.h"
#include "tag_eeprom.h"
#include <string.h>

static void asyncOperationCompleted(int8_t result,void* userData){
    if (userData!=NULL){
        char response[1]="";
        response[0]=result;
        
        ((UartCmdProcesser*)userData)->writeResponse(response,sizeof(response));
    }
}

void tag_uart_cmds_processCmd(UartCmdProcesser* cmd){
    cmd->currentCommand&= ~UART_CMDS_TYPE_TAG;
    switch(cmd->currentCommand){
        case TAG_UART_CMD_WRITE_PAGE:
        {
            uint8_t pageIndex=cmd->cmdBuffer[0];
            short result=tag_ntag213_write_page(pageIndex,cmd->cmdBuffer+1,cmd->cmdBufferPos);
            char data[1]="";
            data[0]=result;
            
            if( result ){
                if( tag_eeprom_write_page(pageIndex,cmd->cmdBuffer+1,NTAG_NUM_BYTES_PER_PAGE) == TAG_EEPROM_ERROR_BUSY){
                    data[0]=TAG_EEPROM_ERROR_BUSY;
                }
            }
            
            cmd->writeResponse(data,sizeof(data));
            break;
        }
        case TAG_UART_CMD_READ_PAGES:
        {
            uint8_t pageIndex=cmd->cmdBuffer[0];
            uint8_t buf[NTAG_NUM_BYTES_PER_PAGE * NTAG_NUM_PAGES_PER_READ]={};
            tag_ntag213_read_pages(pageIndex,buf,NTAG_NUM_BYTES_PER_PAGE * NTAG_NUM_PAGES_PER_READ);
            cmd->writeResponse(buf,sizeof(buf));
            break;
        }
        case TAG_UART_CMD_LAST_AUTH:
        {
            cmd->writeResponse(tag_runtime_data.lastAuthKeySent,NTAG_AUTH_KEY_SIZE);
            break;
        }
        case TAG_UART_CMD_LAST_PAGE_READ:
        {
            uint8_t data[1]="";
            data[0]= tag_runtime_data.lastPageIndexRead;
            
            cmd->writeResponse(data,sizeof(data));
            break;
        }
        case TAG_UART_CMD_LAST_PAGE_WRITE:
        {
            uint8_t data[1 + NTAG_NUM_BYTES_PER_PAGE]="";
            if (tag_runtime_data.lastPageIndexWritten > -1){
                data[0]=tag_runtime_data.lastPageIndexWritten;
                memcpy(data +1,Ntag_pages[tag_runtime_data.lastPageIndexWritten],NTAG_NUM_BYTES_PER_PAGE);
            }
            cmd->writeResponse(data,sizeof(data));
            break;
        }
        case TAG_UART_CMD_RESET_STATS:
            tag_ntag213_reset_status();
            break;
        case TAG_UART_CMD_RELOAD_TAG_DATA:
            tag_runtime_data.currentOperationCompletedCallback=asyncOperationCompleted;
            tag_runtime_data.currentAsyncOperationCompletedCallbackUserData=cmd;
            tag_ntag213_load_Pages_EEPROM_start_load();
            break;
    }
}