#ifndef TAG_EEPROM_H
#define TAG_EEPROM_H

#define TAG_EEPROM_EVENT_WRITE 0x1
#define TAG_EEPROM_EVENT_READ 0x2
#define TAG_EEPROM_ERROR_OK 1
#define TAG_EEPROM_ERROR_BUSY 2
#define TAG_EEPROM_ERROR_READ_FAILED 0

int8_t tag_eeprom_write_page(int8_t pageNum,uint8_t* pageDataBuf,int8_t bufSize);

void tag_eeprom_read_pages(int8_t startPage);

void tag_eeprom_read_page_completed(int8_t errCode,int8_t pageNum,char* pageDataBuf,int8_t bufSize);

void tag_eeprom_Write_completed(int8_t errCode,int8_t pageNum);

#endif