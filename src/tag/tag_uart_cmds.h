#ifndef TAG_UART_CMDS_H
#define TAG_UART_CMDS_H
#include "../uart_cmds.h"
#include "tag_ntag213.h"
#define TAG_UART_CMDS_H
#define TAG_UART_CMD_WRITE_PAGE 1
#define TAG_UART_CMD_READ_PAGES 2
#define TAG_UART_CMD_LAST_AUTH 3
#define TAG_UART_CMD_LAST_PAGE_READ 4
#define TAG_UART_CMD_LAST_PAGE_WRITE 5
#define TAG_UART_CMD_RESET_STATS 6
#define TAG_UART_CMD_RELOAD_TAG_DATA 7
#define TAG_UART_CRC_SIZE 2
static inline short tag_uart_cmds_getRequiredByteCount(uint8_t cmd){
    switch(cmd){
        case TAG_UART_CMD_WRITE_PAGE:
            return NTAG_NUM_BYTES_PER_PAGE + 1;
        case TAG_UART_CMD_READ_PAGES:
            return 2;
        case TAG_UART_CMD_LAST_AUTH:
        case TAG_UART_CMD_LAST_PAGE_READ:
        case TAG_UART_CMD_LAST_PAGE_WRITE:
        case TAG_UART_CMD_RESET_STATS:
            return 0;
        case TAG_UART_CMD_RELOAD_TAG_DATA:
            return 1;
        default:
            return -1;
    }
};
void tag_uart_cmds_processCmd(UartCmdProcesser* cmd);
#endif