#include "tag_ntag213.h"
#include "tag_eeprom.h"
#include "../reader_tag_io_buffer.h"
#include<string.h>

static void tag_ntag213_process_write();
static void tag_ntag213_process_read();
static void tag_ntag213_process_auth();
static void tag_ntag213_write_response(char* buf, unsigned char size);

static int8_t next_page_for_read=-1;

void tag_ntag213_init() {
    tag_runtime_data.currentOperationCompletedCallback=NULL;
    tag_runtime_data.currentAsyncOperationCompletedCallbackUserData=NULL;
    
	tag_runtime_data.state = NTAG_STATE_IDLE;
    memset(tag_runtime_data.lastAuthKeySent,0,NTAG_AUTH_KEY_SIZE);
    
    tag_ntag213_reset_status();
    tag_ntag213_load_Pages_EEPROM_start_load();
}

void tag_ntag213_reset_status(){
    memset(tag_runtime_data.lastAuthKeySent,0,NTAG_AUTH_KEY_SIZE);
    tag_runtime_data.lastPageIndexRead=-1;
    tag_runtime_data.lastPageIndexWritten=-1;
}
void tag_ntag213_process_buffer() {
    /* lock the buffer while we are processing it*/
    reader_tag_io_buffer.bufferState = BUFFER_STATE_BUSY;

    unsigned char cmd = reader_tag_io_buffer_read_byte();
	
	if (cmd == NTAG_CMD_CL1) {
		tag_runtime_data.state = NTAG_STATE_READY1;
		cmd = reader_tag_io_buffer_read_byte();
	}
	else if (cmd == NTAG_CMD_CL2) {
		tag_runtime_data.state = NTAG_STATE_READY2;
		cmd = reader_tag_io_buffer_read_byte();
	}
    
    switch (cmd) {
        case NTAG_CMD_REQA:
        case NTAG_CMD_WUPA:
            TAG_DEBUG_LOG("NTAG_CMD_WUPA");
			char data[2] = "";
			data[0] = (char)NTAG_ATQA_RESPONSE;
			data[1] = 0;
            
			tag_ntag213_write_response(data, 2);
            break;
		case NTAG_CMD_SELECT:
		{
			//tag select
			/* 0x88 -- C1
			   uid0-uid2
			   checksum
			*/

			char data[5]="";
			reader_tag_io_buffer_read(data, 5);

			if ((data[0] ^ data[1] ^ data[2] ^ data[3]) != data[4]) {
				return;
			}
			
            if(tag_runtime_data.state == NTAG_STATE_READY2){
                data[0]=0x0;
            }
			else {
				data[0]=0x04;
			}
            tag_runtime_data.state = MTAG_STATE_ACTIVE;
			tag_ntag213_write_response(data, 1);
			break;
		}
		case NTAG_CMD_ANTICOLLISION:
		{
			if (tag_runtime_data.state == NTAG_STATE_READY1) {

				//respond with Cascade Tag byte and first 3 bytes of uid
				char data[5] = {0x88, 0x00,0x00,0x00,0x0 };
                memcpy(data+1,Ntag_pages[NTAG_UID_PAGE_INDEX_1],3);
                
				//checksum
                data[4]=(data[0] ^ data[1] ^ data[2] ^ data[3]);
				tag_ntag213_write_response(data, 5);
			}
			else if (tag_runtime_data.state = NTAG_STATE_READY2) {
				
				//Cascade level 2 respond with last harf of uid
				char response[5] ={0x00,0x00,0x00,0x00,0x00};
 
                memcpy(response,Ntag_pages[NTAG_UID_PAGE_INDEX_2],NTAG_NUM_BYTES_PER_PAGE);
				//checksum
				response[4]=(response[0] ^ response[1] ^ response[2] ^ response[3]);
				tag_ntag213_write_response(response, 5);
			}
			break;
		}
        case NTAG_CMD_SELECT_HLTA:
            TAG_DEBUG_LOG("NTAG_CMD_SELECT_HLTA");
			tag_runtime_data.state = NTAG_STATE_IDLE;
            break;

        case NTAG_CMD_GET_VERSION:
        {
            //GET Version response for NTAG213
            char response[8]={0x00,0x04,0x04,0x04,0x01,0x00,0x0F,0x03};//crc 0x18,0xAA
            tag_ntag213_write_response(response, sizeof(response));

            break;
        }
        
        case NTAG_CMD_READ:
            TAG_DEBUG_LOG("NTAG_CMD_READ");
            tag_ntag213_process_read();
            break;

        case NTAG_CMD_WRITE:
            TAG_DEBUG_LOG("NTAG_CMD_WRITE");
            tag_ntag213_process_write();
            break;
            
        case NTAG_CMD_PWD_AUTH:
            TAG_DEBUG_LOG("NTAG_CMD_PWD_AUTH");
            tag_ntag213_process_auth();
            break;
        /**
         * commands not currently implemented   
        case NTAG_CMD_COMP_WRITE:
            TAG_DEBUG_LOG("NTAG_CMD_COMP_WRITE");

            break;

        case NTAG_CMD_READ_CNT:
            TAG_DEBUG_LOG("NTAG_CMD_READ_CNT");

            break;
            
        case NTAG_CMD_FAST_READ:
            TAG_DEBUG_LOG("NTAG_CMD_FAST_READ");

            break;

        case NTAG_CMD_READ_SIG:
            TAG_DEBUG_LOG("NTAG_CMD_READ_SIG");
            
            break;
        */
    }

    reader_tag_io_buffer.bufferState = BUFFER_STATE_IDLE;
}

static void tag_ntag213_process_read(){
    uint8_t page_addr=reader_tag_io_buffer_read_byte();

    /* check the address is valid */
    if( page_addr >=  NTAG_NUM_PAGES - NTAG_NUM_PAGES_PER_READ){
        
        return;
    }
    
    uint8_t i;
    for( i=0;i<4;i++){
        tag_ntag213_write_response(Ntag_pages[page_addr+i],NTAG_NUM_BYTES_PER_PAGE);
    }
    tag_runtime_data.lastPageIndexRead=page_addr;
}

char tag_ntag213_write_page(uint8_t pageIndex,char* buf,unsigned char bufSize){
    
    if(pageIndex > NTAG_NUM_PAGES -1 ){
        return 0;
    }
    
    memcpy(Ntag_pages[pageIndex],buf,NTAG_NUM_BYTES_PER_PAGE);
    return 1;
}

uint8_t tag_ntag213_read_pages(uint8_t startPageIndex,char* buf,uint8_t bufSize){

    uint8_t i;
    uint8_t cPageIndex=startPageIndex;
    uint8_t pos=0;
    for( i=0;i<NTAG_NUM_PAGES_PER_READ;i++){
        if( pos+NTAG_NUM_BYTES_PER_PAGE > bufSize){
            break;
        }
        if( cPageIndex > NTAG_NUM_PAGES-1){
            cPageIndex=0;
        }
        memcpy(buf+pos,Ntag_pages[cPageIndex],NTAG_NUM_BYTES_PER_PAGE);
        pos+=NTAG_NUM_BYTES_PER_PAGE;
        cPageIndex++;
    }
    
    return pos;
}

static void tag_ntag213_process_write(){
     uint8_t page_addr=reader_tag_io_buffer_read_byte();
    
    /* check the address is valid */
    if( page_addr >=  NTAG_NUM_PAGES - NTAG_NUM_BYTES_PER_PAGE){
        
        return;
    }
     
    reader_tag_io_buffer_read(Ntag_pages[page_addr],NTAG_NUM_BYTES_PER_PAGE);
    tag_eeprom_write_page(page_addr,Ntag_pages[page_addr],NTAG_NUM_BYTES_PER_PAGE);
    
    char response[1]={NTAG_RESPONSE_ACK};
    tag_ntag213_write_response(response,sizeof(response));
    tag_runtime_data.lastPageIndexWritten=page_addr;
}

static void tag_ntag213_process_auth(){
    char auth[NTAG_AUTH_KEY_SIZE]="";
    
    if( reader_tag_io_buffer_read(auth,NTAG_AUTH_KEY_SIZE) != NTAG_AUTH_KEY_SIZE ){
        return;
    }
    
    memcpy(tag_runtime_data.lastAuthKeySent,auth,NTAG_AUTH_KEY_SIZE);
	/* if the password is correct the PACK is returned */
	tag_ntag213_write_response(Ntag_pages[NTAG_AUTH_PACK_PAGE_INDEX] + NTAG_AUTH_PACK_PAGE_BYTE_INDEX, NTAG_AUTH_PACK_SIZE);
    
}

static void tag_ntag213_write_response(char* buf, unsigned char size){
    reader_tag_io_buffer_write(buf,size);
}

void tag_ntag213_load_Pages_EEPROM_start_load(){
    next_page_for_read=NTAG_NUM_PAGES_PER_READ;
    tag_eeprom_read_pages(0); 
}

void tag_eeprom_Write_completed(int8_t errCode,int8_t pageNum){
    if (tag_runtime_data.currentOperationCompletedCallback != NULL){
       tag_runtime_data.currentOperationCompletedCallback
       (errCode,tag_runtime_data.currentAsyncOperationCompletedCallbackUserData);
   }   
}

void tag_eeprom_read_page_completed(int8_t errCode,int8_t startPageNum,char* pageDataBuf,int8_t bufSize){
    if( errCode == TAG_EEPROM_ERROR_OK && next_page_for_read > -1){

        uint8_t i;
        for(i=0;i<NTAG_NUM_PAGES_PER_READ;i++){
            //save the read data to our memory buffer
            tag_ntag213_write_page(startPageNum + i ,pageDataBuf+(i* NTAG_NUM_BYTES_PER_PAGE),NTAG_NUM_BYTES_PER_PAGE);
           
        }
        if( next_page_for_read > NTAG_NUM_PAGES){
            next_page_for_read=-1;
            if (tag_runtime_data.currentOperationCompletedCallback != NULL){
                tag_runtime_data.currentOperationCompletedCallback
                (errCode,tag_runtime_data.currentAsyncOperationCompletedCallbackUserData);
            }
            return;
        }
        
        tag_eeprom_read_pages(next_page_for_read);
        
        next_page_for_read+=NTAG_NUM_PAGES_PER_READ;

    }
}