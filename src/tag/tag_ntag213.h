#ifndef TAG_NTAG213_H
#define TAG_NTAG213_H
#include "stdint.h"
#define NTAG_NUM_PAGES 45
#define NTAG_NUM_BYTES_PER_PAGE 4
#define NTAG_NUM_PAGES_PER_READ 4
#define NTAG_NUM_BYTES_RETURNED_PER_READ 16
#define NTAG_UID_PART_SIZE 4
#define NTAG_UID_SIZE 8
typedef enum {
	NTAG_STATE_IDLE,
	NTAG_STATE_READY1,
	NTAG_STATE_READY2,
	MTAG_STATE_ACTIVE,
	NTAG_STATE_AUTHENTICATED,
	NTAG_STATE_HALT,

} NTAG_STATE;

#define NTAG_CMD_REQA 0x26
#define NTAG_CMD_WUPA 0x52

#define NTAG_CMD_CL1 0x93
#define NTAG_CMD_CL2 0x95

#define NTAG_CMD_ANTICOLLISION 0x20
#define NTAG_CMD_SELECT 0x70

#define NTAG_CMD_SELECT_HLTA 0x50
#define NTAG_CMD_GET_VERSION 0x60
#define NTAG_CMD_READ 0x30
#define NTAG_CMD_FAST_READ 0x3A
#define NTAG_CMD_WRITE 0xA2
#define NTAG_CMD_COMP_WRITE 0xA0
#define NTAG_CMD_READ_CNT 0x39
#define NTAG_CMD_PWD_AUTH 0x1B
#define NTAG_CMD_READ_SIG 0x3C


#define NTAG_ATQA_RESPONSE 0x44

#define NTAG_AUTH_KEY_SIZE 4
#define NTAG_AUTH_PACK_PAGE_INDEX NTAG_NUM_PAGES-1
#define NTAG_AUTH_PACK_PAGE_BYTE_INDEX 0
#define NTAG_AUTH_PACK_SIZE 2

#define NTAG_UID_PAGE_INDEX_1 0
#define NTAG_UID_PAGE_INDEX_2 1
#define NTAG_UID_PAGE_INDEX_3 2

extern char Ntag_pages[NTAG_NUM_PAGES][NTAG_NUM_BYTES_PER_PAGE];

typedef void (*AsyncOperationCompleted) (int8_t result_code,void* userdata);

typedef struct {
    uint8_t lastAuthKeySent[NTAG_AUTH_KEY_SIZE];
    int8_t lastPageIndexRead;
    int8_t lastPageIndexWritten;
    NTAG_STATE state;
    void* currentAsyncOperationCompletedCallbackUserData;
    AsyncOperationCompleted currentOperationCompletedCallback;
} NTag;

NTag tag_runtime_data;
;
#define NTAG_RESPONSE_ACK 0xA
#define NTAG_RESPONSE_NACK_INVALID_ARG 0x0
#define NTAG_RESPONSE_NACK_CRC_ERROR 0x1
#define NTAG_RESPONSE_NACK_INVALIID_AUTH 0x4
#define NTAG_RESPONSE_NACK_EEPROM_WRITE_ERROR 0x5

char tag_ntag213_write_page(uint8_t pageIndex,char* buf,unsigned char bufSize);

uint8_t tag_ntag213_read_pages(uint8_t startPageIndex,char* buf,uint8_t bufSize);

void tag_ntag213_process_buffer();

void tag_ntag213_reset_status();

void tag_ntag213_load_Pages_EEPROM_start_load();

void tag_ntag213_init();

#define TAG_DEBUG_LOG(MSG) /*printf("TAG DEBUG:%s\n",MSG);*/
#endif