/**
 *  
 *  credit https://stackoverflow.com/questions/40869293/iso-iec-14443a-crc-calcuation
 */
 #ifndef ISO14443A_CRC_H
 #define ISO14443A_CRC_H
#include<stdint.h>
 void iso14443a_crc(uint8_t* pbtData, uint8_t szLen, uint8_t* pbtCrc);
 
 #endif