/**
 *  circular buffer impl from http://embedjournal.com/implementing-circular-buffer-embedded-c/
 */
#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H
#include<stdint.h>
typedef struct {
    uint8_t * const buffer;
    int head;
    int tail;
    const int maxLen;
} circBuf_t;

int inline circular_buffer_circBufPop(circBuf_t *c, uint8_t *data);
int inline circular_buffer_circBufPush(circBuf_t *c, uint8_t data);

#define CIRCBUF_DEF(x,y)          \
    uint8_t x##_dataSpace[y];     \
    circBuf_t x = {               \
        .buffer = x##_space,      \
        .head = 0,                \
        .tail = 0,                \
        .maxLen = y               \
    }
#endif
