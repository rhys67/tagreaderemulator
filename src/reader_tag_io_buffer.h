
#ifndef READER_TAG_IO_BUFFER_H
#define READER_TAG_IO_BUFFER_H
#include <stdint.h>
#include "3party/circular_buffer.h"
#define FIFO_BUFFER_SIZE 64

typedef enum {
    BUFFER_STATE_BUSY,
    BUFFER_STATE_NOTIFY_TAG_READ,
    BUFFER_STATE_NOTIFY_TAG_WRITE,
    BUFFER_STATE_NOTIFY_READER,
    BUFFER_STATE_IDLE
} PN_FIFO_BUFFER_STATE;

typedef struct{
    char buf[FIFO_BUFFER_SIZE];
    uint8_t head;
    uint8_t tail;
    uint8_t size;
    uint8_t usedCount;
    PN_FIFO_BUFFER_STATE bufferState;
    
} PN_FIFO_BUFFER;

PN_FIFO_BUFFER reader_tag_io_buffer;

extern uint8_t tag_reader_FIFO_buffer[FIFO_BUFFER_SIZE+1];
extern circBuf_t tag_reader_FIFO;

uint8_t reader_tag_io_buffer_read(char * buf, uint8_t nbytes);
uint8_t reader_tag_io_buffer_write(char * buf, uint8_t nbytes);

void reader_tag_io_buffer_write_byte(char c);
uint8_t reader_tag_io_buffer_read_byte();
uint8_t reader_tag_io_buffer_getCrc(uint8_t* crcBuf);

void reader_tag_io_buffer_clear();
unsigned char reader_tag_io_buffer_level();
#endif