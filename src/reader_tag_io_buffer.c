#include "reader_tag_io_buffer.h"
#include "3party/iso14443a_crc.h"

uint8_t tag_reader_FIFO_buffer[FIFO_BUFFER_SIZE + 1];
circBuf_t tag_reader_FIFO = { tag_reader_FIFO_buffer,0,0,FIFO_BUFFER_SIZE };

uint8_t reader_tag_io_buffer_read(char * buf, uint8_t nbytes){
    short i;
    char * p;
    p=buf;
    for(i=0;i<nbytes;i++){
        if(circular_buffer_circBufPop(&tag_reader_FIFO,p++)) break;
    }   
    return i;
}

uint8_t reader_tag_io_buffer_write(char * buf, uint8_t nbytes){
    short i;
    char * p;
    p=buf;
    for(i=0;i<nbytes;i++){
        if(circular_buffer_circBufPush(&tag_reader_FIFO,*p++)) break;
    }
    return i;
}

void reader_tag_io_buffer_write_byte(char c){
    circular_buffer_circBufPush(&tag_reader_FIFO,c);
}

uint8_t reader_tag_io_buffer_read_byte(){
    uint8_t buf=0;
    circular_buffer_circBufPop(&tag_reader_FIFO,&buf);
    
    return buf;
}

uint8_t reader_tag_io_buffer_peek(uint8_t* buf,uint8_t bufSize){

    circBuf_t* c=&tag_reader_FIFO;
    
    uint8_t tmpTail=c->tail;
    uint8_t numRead=0;
    
    while(numRead<bufSize){
        // if the head isn't ahead of the tail, we don't have any characters
        if (c->head == tmpTail) // check if circular buffer is empty
            return numRead;          // and return with an error

        // next is where tail will point to after this read.
        int next = tmpTail + 1;
        if(next >= c->maxLen)
            next = 0;

        *buf++ = c->buffer[tmpTail]; // Read data and then move
        tmpTail = next;             // tail to next data offset.
        numRead++;
    }
    
    return numRead;  
}

uint8_t reader_tag_io_buffer_getCrc(uint8_t* crcBuf){
    //for now this will do... it's mainly to handle the case of wraparound without refactoring the crc method
    uint8_t bufferCopy[FIFO_BUFFER_SIZE];
    
    uint8_t numBytesRead=reader_tag_io_buffer_peek(bufferCopy,FIFO_BUFFER_SIZE);
    if( numBytesRead == 0){
        return 1;
    }
    
    iso14443a_crc(bufferCopy,numBytesRead,crcBuf);
    
    return 0;
}

void reader_tag_io_buffer_clear(){
    tag_reader_FIFO.head=0;
    tag_reader_FIFO.tail=0;
}

unsigned char reader_tag_io_buffer_level(){
	return tag_reader_FIFO.maxLen-((tag_reader_FIFO.tail > tag_reader_FIFO.head)
                ? tag_reader_FIFO.tail-tag_reader_FIFO.head
                : tag_reader_FIFO.maxLen - tag_reader_FIFO.head + tag_reader_FIFO.tail);
}