#ifndef UART_CMDS_H
#define UART_CMDS_H
#include<stdint.h>
#include "../src/reader/reader_pns512.h"
#define UART_CMDS_TYPE_READER 0x80
#define UART_CMDS_TYPE_TAG 0x40
#define UART_CMDS_TYPE_NONE 0x0
#define UART_CMDS_NO_ARGS_REQUIRED 0
#define UART_CMDS_UNKNOWN -1
#define UART_CMD_BUFFER_SIZE 64

typedef void (*WriteCommandResponse) (char* responseData, short len);

typedef struct{
   char currentCommand;
   char cmdBuffer[UART_CMD_BUFFER_SIZE];
   uint8_t cmdBufferPos;
   uint8_t cmdRequiredBytes;
   WriteCommandResponse writeResponse;
   PNSReader* reader;
} UartCmdProcesser;

void uart_cmds_readByte(UartCmdProcesser* state,int8_t b);

#endif