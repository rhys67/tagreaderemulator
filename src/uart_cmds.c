#include "uart_cmds.h"
#include "reader/reader_uart_cmds.h"
#include "tag/tag_uart_cmds.h"

void uart_cmds_readByte(UartCmdProcesser* state,int8_t b){
    if( state->currentCommand == UART_CMDS_TYPE_NONE){
        char cmd=b;
        short numBytesToBuffer=-1;
        state->currentCommand=b;
        if( b & UART_CMDS_TYPE_READER){
            cmd&= ~UART_CMDS_TYPE_READER;
            numBytesToBuffer= reader_uart_cmds_getRequiredByteCount(cmd);
            if( numBytesToBuffer == UART_CMDS_NO_ARGS_REQUIRED){
                reader_uart_cmds_processCmd(state);
                state->currentCommand=UART_CMDS_TYPE_NONE;
                return;
            }
        }
        else if( b & UART_CMDS_TYPE_TAG){
            cmd&= ~UART_CMDS_TYPE_TAG;
            numBytesToBuffer= tag_uart_cmds_getRequiredByteCount(cmd);
            if( numBytesToBuffer == UART_CMDS_NO_ARGS_REQUIRED){
                tag_uart_cmds_processCmd(state);
                state->currentCommand=UART_CMDS_TYPE_NONE;
                return;
            }
        }
        
        if( numBytesToBuffer != UART_CMDS_UNKNOWN){
            state->currentCommand =b;
            state->cmdBufferPos=0;
            state->cmdRequiredBytes=numBytesToBuffer;
            
        }else{
            char data[1]={UART_CMDS_UNKNOWN};
            state->writeResponse(data,sizeof(data));
        }
    }else{
        if( state->cmdBufferPos<state->cmdRequiredBytes && state->cmdBufferPos<UART_CMD_BUFFER_SIZE ){
            state->cmdBuffer[state->cmdBufferPos++]=b;
        }
        if(state->cmdBufferPos>=state->cmdRequiredBytes){
            int8_t cmd=state->currentCommand;
            if( cmd & UART_CMDS_TYPE_READER){
                reader_uart_cmds_processCmd(state);
            }
            else if( cmd & UART_CMDS_TYPE_TAG){
                tag_uart_cmds_processCmd(state);
            }
            state->currentCommand=UART_CMDS_TYPE_NONE;
        }
    }
}