#ifndef READER_CONFIG_H
#define READER_CONFIG_H
#include <stdint.h>

typedef void (*GetConfigCompleted) (int8_t errCode,uint8_t data, void* context);
typedef void (*SetConfigCompleted) (int8_t errCode);

void reader_config_get(int8_t configItemIndex,GetConfigCompleted callback,void* context);

void reader_config_set(int8_t configItemIndex,uint8_t data,SetConfigCompleted callback);
#endif